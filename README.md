# Lab5 -- Integration testing

## ===================
## SOLUTION:

Specs url: https://script.google.com/macros/s/AKfycbwI_VT8ONTc6mL7CYzILU8n7BxF1pa0BD9L-X5ta6hMBoscDfw0jvpD8g/exec?service=getSpec&email=d.kochetov@innopolis.ru

```bash
Here is InnoCar Specs:
Budet car price per minute = 15
Luxury car price per minute = 31
Fixed price per km = 13
Allowed deviations in % = 16
Inno discount in % = 9
```

Decision table:

| Conditions(inputs) | Values                            | R1          | R2          | R3     | R4     | R5     | R6     | R7          | R8          | R9       | R10      | R11 | R12 | R13 | R14 | R15      |
|--------------------|-----------------------------------|-------------|-------------|--------|--------|--------|--------|-------------|-------------|----------|----------|-----|-----|-----|-----|----------|
| Type               | "budget","luxury",nonsense        | budget      | budget      | budget | budget | luxury | luxury | luxury      | luxury      | nonsense | *        | *   | *   | *   | *   | *        |
| Plan               | "minute","fixed_price",nonsense   | fixed_price | fixed_price | minute | minute | minute | minute | fixed_price | fixed_price | *        | nonsense | *   | *   | *   | *   | *        |
| Distance           | >0, <=0                           | >0          | >0          | >0     | >0     | >0     | >0     | >0          | >0          | *        | *        | <=0 | *   | *   | *   | *        |
| Planned Distance   | >0, <=0                           | >0          | >0          | >0     | >0     | >0     | >0     | >0          | >0          | *        | *        | *   | <=0 | *   | *   | *        |
| Time               | >0, <=0                           | >0          | >0          | >0     | >0     | >0     | >0     | >0          | >0          | *        | *        | *   | *   | <=0 | *   | *        |
| PlannedTime        | >0, <=0                           | >0          | >0          | >0     | >0     | >0     | >0     | >0          | >0          | *        | *        | *   | *   | *   | <=0 | *        |
| Discount           | "yes","no",nonsense               | "yes"       | "no"        | "yes"  | "no"   | "yes"  | "no"   | "yes"       | "no"        | *        | *        | *   | *   | *   | *   | nonsense |
| ----------         | ----------                        | --          |  --         | --     | --     | --     | --     |      --     | --          | --       |  --      |--   | --  | --  | --  | --       |
| Invalid Request    |                                   |             |             |        |        |        |        | X           | X           | X        | X        | X   | X   | X   | X   | X        |
| 200                |                                   | X           | X           | X      | X      | X      | X      |             |             |          |          |     |     |     |     |          |

Test cases:

| Test case | Type | Plan | Distance | Planned Distance | Time | Planned Time | Discount | Result | Expected |
|---|---|---|---|---|---|---|---|---|---|
| 0 | luxury | minute | 0 | 0 | 0 | 0 | yes | 0.00 | Invalid Request |
| 1 | luxury | minute | 0 | 0 | 0 | 0 | no | 0.00 | Invalid Request |
| 2 | luxury | minute | 0 | 0 | 0 | 1 | yes | 0.00 | Invalid Request |
| 3 | luxury | minute | 0 | 0 | 0 | 1 | no | 0.00 | Invalid Request |
| 4 | luxury | minute | 0 | 0 | 1 | 0 | yes | 19.75 | Invalid Request |
| 5 | luxury | minute | 0 | 0 | 1 | 0 | no | 21.70 | Invalid Request |
| 6 | luxury | minute | 0 | 0 | 1 | 1 | yes | 19.75 | Invalid Request |
| 7 | luxury | minute | 0 | 0 | 1 | 1 | no | 21.70 | Invalid Request |
| 8 | luxury | minute | 0 | 1 | 0 | 0 | yes | 0.00 | Invalid Request |
| 9 | luxury | minute | 0 | 1 | 0 | 0 | no | 0.00 | Invalid Request |
| 10 | luxury | minute | 0 | 1 | 0 | 1 | yes | 0.00 | Invalid Request |
| 11 | luxury | minute | 0 | 1 | 0 | 1 | no | 0.00 | Invalid Request |
| 12 | luxury | minute | 0 | 1 | 1 | 0 | yes | 19.75 | Invalid Request |
| 13 | luxury | minute | 0 | 1 | 1 | 0 | no | 21.70 | Invalid Request |
| 14 | luxury | minute | 0 | 1 | 1 | 1 | yes | 19.75 | Invalid Request |
| 15 | luxury | minute | 0 | 1 | 1 | 1 | no | 21.70 | Invalid Request |
| 16 | luxury | minute | 1 | 0 | 0 | 0 | yes | 0.00 | Invalid Request |
| 17 | luxury | minute | 1 | 0 | 0 | 0 | no | 0.00 | Invalid Request |
| 18 | luxury | minute | 1 | 0 | 0 | 1 | yes | 0.00 | Invalid Request |
| 19 | luxury | minute | 1 | 0 | 0 | 1 | no | 0.00 | Invalid Request |
| 20 | luxury | minute | 1 | 0 | 1 | 0 | yes | 19.75 | Invalid Request |
| 21 | luxury | minute | 1 | 0 | 1 | 0 | no | 21.70 | Invalid Request |
| 22 | luxury | minute | 1 | 0 | 1 | 1 | yes | 19.75 | Invalid Request |
| 23 | luxury | minute | 1 | 0 | 1 | 1 | no | 21.70 | Invalid Request |
| 24 | luxury | minute | 1 | 1 | 0 | 0 | yes | 0.00 | Invalid Request |
| 25 | luxury | minute | 1 | 1 | 0 | 0 | no | 0.00 | Invalid Request |
| 26 | luxury | minute | 1 | 1 | 0 | 1 | yes | 0.00 | Invalid Request |
| 27 | luxury | minute | 1 | 1 | 0 | 1 | no | 0.00 | Invalid Request |
| 28 | luxury | minute | 1 | 1 | 1 | 0 | yes | 19.75 | Invalid Request |
| 29 | luxury | minute | 1 | 1 | 1 | 0 | no | 21.70 | Invalid Request |
| 30 | luxury | minute | 1 | 1 | 1 | 1 | yes | 19.75 | 28.21 |
| 31 | luxury | minute | 1 | 1 | 1 | 1 | no | 21.70 | 31.00 |
| 32 | luxury | fixed_price | 0 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 33 | luxury | fixed_price | 0 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 34 | luxury | fixed_price | 0 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 35 | luxury | fixed_price | 0 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 36 | luxury | fixed_price | 0 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 37 | luxury | fixed_price | 0 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 38 | luxury | fixed_price | 0 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 39 | luxury | fixed_price | 0 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 40 | luxury | fixed_price | 0 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 41 | luxury | fixed_price | 0 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 42 | luxury | fixed_price | 0 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 43 | luxury | fixed_price | 0 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 44 | luxury | fixed_price | 0 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 45 | luxury | fixed_price | 0 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 46 | luxury | fixed_price | 0 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 47 | luxury | fixed_price | 0 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 48 | luxury | fixed_price | 1 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 49 | luxury | fixed_price | 1 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 50 | luxury | fixed_price | 1 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 51 | luxury | fixed_price | 1 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 52 | luxury | fixed_price | 1 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 53 | luxury | fixed_price | 1 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 54 | luxury | fixed_price | 1 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 55 | luxury | fixed_price | 1 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 56 | luxury | fixed_price | 1 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 57 | luxury | fixed_price | 1 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 58 | luxury | fixed_price | 1 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 59 | luxury | fixed_price | 1 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 60 | luxury | fixed_price | 1 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 61 | luxury | fixed_price | 1 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 62 | luxury | fixed_price | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 63 | luxury | fixed_price | 1 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 64 | luxury | nonsense | 0 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 65 | luxury | nonsense | 0 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 66 | luxury | nonsense | 0 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 67 | luxury | nonsense | 0 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 68 | luxury | nonsense | 0 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 69 | luxury | nonsense | 0 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 70 | luxury | nonsense | 0 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 71 | luxury | nonsense | 0 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 72 | luxury | nonsense | 0 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 73 | luxury | nonsense | 0 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 74 | luxury | nonsense | 0 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 75 | luxury | nonsense | 0 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 76 | luxury | nonsense | 0 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 77 | luxury | nonsense | 0 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 78 | luxury | nonsense | 0 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 79 | luxury | nonsense | 0 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 80 | luxury | nonsense | 1 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 81 | luxury | nonsense | 1 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 82 | luxury | nonsense | 1 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 83 | luxury | nonsense | 1 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 84 | luxury | nonsense | 1 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 85 | luxury | nonsense | 1 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 86 | luxury | nonsense | 1 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 87 | luxury | nonsense | 1 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 88 | luxury | nonsense | 1 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 89 | luxury | nonsense | 1 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 90 | luxury | nonsense | 1 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 91 | luxury | nonsense | 1 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 92 | luxury | nonsense | 1 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 93 | luxury | nonsense | 1 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 94 | luxury | nonsense | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 95 | luxury | nonsense | 1 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 96 | budget | minute | 0 | 0 | 0 | 0 | yes | 0.00 | Invalid Request |
| 97 | budget | minute | 0 | 0 | 0 | 0 | no | 0.00 | Invalid Request |
| 98 | budget | minute | 0 | 0 | 0 | 1 | yes | 0.00 | Invalid Request |
| 99 | budget | minute | 0 | 0 | 0 | 1 | no | 0.00 | Invalid Request |
| 100 | budget | minute | 0 | 0 | 1 | 0 | yes | 13.65 | Invalid Request |
| 101 | budget | minute | 0 | 0 | 1 | 0 | no | 15.00 | Invalid Request |
| 102 | budget | minute | 0 | 0 | 1 | 1 | yes | 13.65 | Invalid Request |
| 103 | budget | minute | 0 | 0 | 1 | 1 | no | 15.00 | Invalid Request |
| 104 | budget | minute | 0 | 1 | 0 | 0 | yes | 0.00 | Invalid Request |
| 105 | budget | minute | 0 | 1 | 0 | 0 | no | 0.00 | Invalid Request |
| 106 | budget | minute | 0 | 1 | 0 | 1 | yes | 0.00 | Invalid Request |
| 107 | budget | minute | 0 | 1 | 0 | 1 | no | 0.00 | Invalid Request |
| 108 | budget | minute | 0 | 1 | 1 | 0 | yes | 13.65 | Invalid Request |
| 109 | budget | minute | 0 | 1 | 1 | 0 | no | 15.00 | Invalid Request |
| 110 | budget | minute | 0 | 1 | 1 | 1 | yes | 13.65 | Invalid Request |
| 111 | budget | minute | 0 | 1 | 1 | 1 | no | 15.00 | Invalid Request |
| 112 | budget | minute | 1 | 0 | 0 | 0 | yes | 0.00 | Invalid Request |
| 113 | budget | minute | 1 | 0 | 0 | 0 | no | 0.00 | Invalid Request |
| 114 | budget | minute | 1 | 0 | 0 | 1 | yes | 0.00 | Invalid Request |
| 115 | budget | minute | 1 | 0 | 0 | 1 | no | 0.00 | Invalid Request |
| 116 | budget | minute | 1 | 0 | 1 | 0 | yes | 13.65 | Invalid Request |
| 117 | budget | minute | 1 | 0 | 1 | 0 | no | 15.00 | Invalid Request |
| 118 | budget | minute | 1 | 0 | 1 | 1 | yes | 13.65 | Invalid Request |
| 119 | budget | minute | 1 | 0 | 1 | 1 | no | 15.00 | Invalid Request |
| 120 | budget | minute | 1 | 1 | 0 | 0 | yes | 0.00 | Invalid Request |
| 121 | budget | minute | 1 | 1 | 0 | 0 | no | 0.00 | Invalid Request |
| 122 | budget | minute | 1 | 1 | 0 | 1 | yes | 0.00 | Invalid Request |
| 123 | budget | minute | 1 | 1 | 0 | 1 | no | 0.00 | Invalid Request |
| 124 | budget | minute | 1 | 1 | 1 | 0 | yes | 13.65 | Invalid Request |
| 125 | budget | minute | 1 | 1 | 1 | 0 | no | 15.00 | Invalid Request |
| 126 | budget | minute | 1 | 1 | 1 | 1 | yes | 13.65 | 13.65 |
| 127 | budget | minute | 1 | 1 | 1 | 1 | no | 15.00 | 15.00 |
| 128 | budget | fixed_price | 0 | 0 | 0 | 0 | yes | 0.00 | Invalid Request |
| 129 | budget | fixed_price | 0 | 0 | 0 | 0 | no | 0.00 | Invalid Request |
| 130 | budget | fixed_price | 0 | 0 | 0 | 1 | yes | 0.00 | Invalid Request |
| 131 | budget | fixed_price | 0 | 0 | 0 | 1 | no | 0.00 | Invalid Request |
| 132 | budget | fixed_price | 0 | 0 | 1 | 0 | yes | 15.17 | Invalid Request |
| 133 | budget | fixed_price | 0 | 0 | 1 | 0 | no | 16.67 | Invalid Request |
| 134 | budget | fixed_price | 0 | 0 | 1 | 1 | yes | 15.17 | Invalid Request |
| 135 | budget | fixed_price | 0 | 0 | 1 | 1 | no | 16.67 | Invalid Request |
| 136 | budget | fixed_price | 0 | 1 | 0 | 0 | yes | 0.00 | Invalid Request |
| 137 | budget | fixed_price | 0 | 1 | 0 | 0 | no | 0.00 | Invalid Request |
| 138 | budget | fixed_price | 0 | 1 | 0 | 1 | yes | 0.00 | Invalid Request |
| 139 | budget | fixed_price | 0 | 1 | 0 | 1 | no | 0.00 | Invalid Request |
| 140 | budget | fixed_price | 0 | 1 | 1 | 0 | yes | 15.17 | Invalid Request |
| 141 | budget | fixed_price | 0 | 1 | 1 | 0 | no | 16.67 | Invalid Request |
| 142 | budget | fixed_price | 0 | 1 | 1 | 1 | yes | 15.17 | Invalid Request |
| 143 | budget | fixed_price | 0 | 1 | 1 | 1 | no | 16.67 | Invalid Request |
| 144 | budget | fixed_price | 1 | 0 | 0 | 0 | yes | 0.00 | Invalid Request |
| 145 | budget | fixed_price | 1 | 0 | 0 | 0 | no | 0.00 | Invalid Request |
| 146 | budget | fixed_price | 1 | 0 | 0 | 1 | yes | 0.00 | Invalid Request |
| 147 | budget | fixed_price | 1 | 0 | 0 | 1 | no | 0.00 | Invalid Request |
| 148 | budget | fixed_price | 1 | 0 | 1 | 0 | yes | 15.17 | Invalid Request |
| 149 | budget | fixed_price | 1 | 0 | 1 | 0 | no | 16.67 | Invalid Request |
| 150 | budget | fixed_price | 1 | 0 | 1 | 1 | yes | 15.17 | Invalid Request |
| 151 | budget | fixed_price | 1 | 0 | 1 | 1 | no | 16.67 | Invalid Request |
| 152 | budget | fixed_price | 1 | 1 | 0 | 0 | yes | 0.00 | Invalid Request |
| 153 | budget | fixed_price | 1 | 1 | 0 | 0 | no | 0.00 | Invalid Request |
| 154 | budget | fixed_price | 1 | 1 | 0 | 1 | yes | 0.00 | Invalid Request |
| 155 | budget | fixed_price | 1 | 1 | 0 | 1 | no | 0.00 | Invalid Request |
| 156 | budget | fixed_price | 1 | 1 | 1 | 0 | yes | 15.17 | Invalid Request |
| 157 | budget | fixed_price | 1 | 1 | 1 | 0 | no | 16.67 | Invalid Request |
| 158 | budget | fixed_price | 1 | 1 | 1 | 1 | yes | 11.38 | 11.83 |
| 159 | budget | fixed_price | 1 | 1 | 1 | 1 | no | 12.50 | 13.00 |
| 160 | budget | nonsense | 0 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 161 | budget | nonsense | 0 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 162 | budget | nonsense | 0 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 163 | budget | nonsense | 0 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 164 | budget | nonsense | 0 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 165 | budget | nonsense | 0 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 166 | budget | nonsense | 0 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 167 | budget | nonsense | 0 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 168 | budget | nonsense | 0 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 169 | budget | nonsense | 0 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 170 | budget | nonsense | 0 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 171 | budget | nonsense | 0 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 172 | budget | nonsense | 0 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 173 | budget | nonsense | 0 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 174 | budget | nonsense | 0 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 175 | budget | nonsense | 0 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 176 | budget | nonsense | 1 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 177 | budget | nonsense | 1 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 178 | budget | nonsense | 1 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 179 | budget | nonsense | 1 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 180 | budget | nonsense | 1 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 181 | budget | nonsense | 1 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 182 | budget | nonsense | 1 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 183 | budget | nonsense | 1 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 184 | budget | nonsense | 1 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 185 | budget | nonsense | 1 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 186 | budget | nonsense | 1 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 187 | budget | nonsense | 1 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 188 | budget | nonsense | 1 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 189 | budget | nonsense | 1 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 190 | budget | nonsense | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 191 | budget | nonsense | 1 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 192 | nonsense | minute | 0 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 193 | nonsense | minute | 0 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 194 | nonsense | minute | 0 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 195 | nonsense | minute | 0 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 196 | nonsense | minute | 0 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 197 | nonsense | minute | 0 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 198 | nonsense | minute | 0 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 199 | nonsense | minute | 0 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 200 | nonsense | minute | 0 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 201 | nonsense | minute | 0 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 202 | nonsense | minute | 0 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 203 | nonsense | minute | 0 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 204 | nonsense | minute | 0 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 205 | nonsense | minute | 0 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 206 | nonsense | minute | 0 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 207 | nonsense | minute | 0 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 208 | nonsense | minute | 1 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 209 | nonsense | minute | 1 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 210 | nonsense | minute | 1 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 211 | nonsense | minute | 1 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 212 | nonsense | minute | 1 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 213 | nonsense | minute | 1 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 214 | nonsense | minute | 1 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 215 | nonsense | minute | 1 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 216 | nonsense | minute | 1 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 217 | nonsense | minute | 1 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 218 | nonsense | minute | 1 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 219 | nonsense | minute | 1 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 220 | nonsense | minute | 1 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 221 | nonsense | minute | 1 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 222 | nonsense | minute | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 223 | nonsense | minute | 1 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 224 | nonsense | fixed_price | 0 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 225 | nonsense | fixed_price | 0 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 226 | nonsense | fixed_price | 0 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 227 | nonsense | fixed_price | 0 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 228 | nonsense | fixed_price | 0 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 229 | nonsense | fixed_price | 0 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 230 | nonsense | fixed_price | 0 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 231 | nonsense | fixed_price | 0 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 232 | nonsense | fixed_price | 0 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 233 | nonsense | fixed_price | 0 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 234 | nonsense | fixed_price | 0 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 235 | nonsense | fixed_price | 0 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 236 | nonsense | fixed_price | 0 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 237 | nonsense | fixed_price | 0 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 238 | nonsense | fixed_price | 0 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 239 | nonsense | fixed_price | 0 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 240 | nonsense | fixed_price | 1 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 241 | nonsense | fixed_price | 1 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 242 | nonsense | fixed_price | 1 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 243 | nonsense | fixed_price | 1 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 244 | nonsense | fixed_price | 1 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 245 | nonsense | fixed_price | 1 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 246 | nonsense | fixed_price | 1 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 247 | nonsense | fixed_price | 1 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 248 | nonsense | fixed_price | 1 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 249 | nonsense | fixed_price | 1 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 250 | nonsense | fixed_price | 1 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 251 | nonsense | fixed_price | 1 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 252 | nonsense | fixed_price | 1 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 253 | nonsense | fixed_price | 1 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 254 | nonsense | fixed_price | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 255 | nonsense | fixed_price | 1 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 256 | nonsense | nonsense | 0 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 257 | nonsense | nonsense | 0 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 258 | nonsense | nonsense | 0 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 259 | nonsense | nonsense | 0 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 260 | nonsense | nonsense | 0 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 261 | nonsense | nonsense | 0 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 262 | nonsense | nonsense | 0 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 263 | nonsense | nonsense | 0 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 264 | nonsense | nonsense | 0 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 265 | nonsense | nonsense | 0 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 266 | nonsense | nonsense | 0 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 267 | nonsense | nonsense | 0 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 268 | nonsense | nonsense | 0 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 269 | nonsense | nonsense | 0 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 270 | nonsense | nonsense | 0 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 271 | nonsense | nonsense | 0 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |
| 272 | nonsense | nonsense | 1 | 0 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 273 | nonsense | nonsense | 1 | 0 | 0 | 0 | no | Invalid Request | Invalid Request |
| 274 | nonsense | nonsense | 1 | 0 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 275 | nonsense | nonsense | 1 | 0 | 0 | 1 | no | Invalid Request | Invalid Request |
| 276 | nonsense | nonsense | 1 | 0 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 277 | nonsense | nonsense | 1 | 0 | 1 | 0 | no | Invalid Request | Invalid Request |
| 278 | nonsense | nonsense | 1 | 0 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 279 | nonsense | nonsense | 1 | 0 | 1 | 1 | no | Invalid Request | Invalid Request |
| 280 | nonsense | nonsense | 1 | 1 | 0 | 0 | yes | Invalid Request | Invalid Request |
| 281 | nonsense | nonsense | 1 | 1 | 0 | 0 | no | Invalid Request | Invalid Request |
| 282 | nonsense | nonsense | 1 | 1 | 0 | 1 | yes | Invalid Request | Invalid Request |
| 283 | nonsense | nonsense | 1 | 1 | 0 | 1 | no | Invalid Request | Invalid Request |
| 284 | nonsense | nonsense | 1 | 1 | 1 | 0 | yes | Invalid Request | Invalid Request |
| 285 | nonsense | nonsense | 1 | 1 | 1 | 0 | no | Invalid Request | Invalid Request |
| 286 | nonsense | nonsense | 1 | 1 | 1 | 1 | yes | Invalid Request | Invalid Request |
| 287 | nonsense | nonsense | 1 | 1 | 1 | 1 | no | Invalid Request | Invalid Request |

Conclusion:
- wtf is PlannedTime, it's not mentioned anywhere
- Tests 126,127,158 work as expected. 
- 29, 30 and 157 return something weird.
- All other tests that return a value instead of expected 'Invalid Request' show us that the system accepts '0' as a value for int args that is not appropriate according to the task.


## ===================

## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab

Ok, finally, we won't need to write any code today :). Hope you're happy)
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab5-integration-testing)
2. This time around we have our own variants to test, open [This](https://docs.google.com/spreadsheets/d/1lLigF6um8pKH2IGZNt9xXcl8quPtFPzmVtt5RCLeCqw/edit?usp=sharing) and use your link:
- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman or you may use your browser), install it.
- Now, let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans. With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price` plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to Innopolis residents.
- We will send this request:
    `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_` 
  This will return our spec:
        Here is InnoCar Specs:
        Budet car price per minute = 17
        Luxury car price per minute = 33
        Fixed price per km = 11
        Allowed deviations in % = 10
        Inno discount in % = 10
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters: 
`distance` and `type`.
 `distance` is an integer value, so we can build 2 equivalence classes:
 + `distance` <= 0
 + `distance` > 0
 while `type` have 3 equivalence classes:
 + `budget` 
 + `luxury`
 + or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its value: 
 + `distance` <= 0 : -10 0
 + `distance` > 0 : 1 100000
This way we will test both our borders and our normal values, we can do the same thing for `type`:
 + `type` = "budget" 
 + `type` = "luxury" 
 + `type` = "Guten morgen sonnenschein" 

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we will have:
  + `plan` = `minute` 
  + `planned_distance` = `100` 
  + `time` = `110` 
  + `planned_time` = `100` 
  + `inno_discount` = `yes`
Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|---|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools.
Let's use Decision table to cut out our tests:
| Conditions(inputs)  |             Values           |    R1    |   R2    |   R3  |    R4   |
|---------------------|------------------------------|----------|---------|-------|---------|
|     Type            | "budget","luxury", nonsense  | nonsense |  budget | luxury|    *    |
|     Distance        | >0, <=0                      |     *    |  >0     |  >0   |    <=0  |
|---------------------|------------------------------|----------|---------|-------|---------|
|  Invalid Request    |                              |    X     |         |       |    X    |
|     200             |                              |          |    X    |   X   |         |

Now let's use this request to test our values:
`https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_`
Ok, then we can write just one test to test nonsense and few tests for distance <= 0 and 4 tests to check correct work of the system, let's send requests to checck that.




## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 

