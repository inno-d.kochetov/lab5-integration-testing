package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
)

const budgetPriceMin = 15
const luxuryPriceMin = 31
const fixedPriceKm = 13
const DeviationPercent = 16
const Discount = 9
const urlTemplate = "https://script.google.com/macros/s/AKfycbwI_VT8ONTc6mL7CYzILU8n7BxF1pa0BD9L-X5ta6hMBoscDfw0jvpD8g/exec?" +
	"service=calculatePrice&" +
	"email=d.kochetov@innopolis.ru&" +
	"type=%s&" +
	"plan=%s&" +
	"distance=%d&" +
	"planned_distance=%d&" +
	"time=%d&" +
	"planned_time=%d&" +
	"inno_discount=%s"

type Query struct {
	tp         string
	plan       string
	distance   int
	plDistance int
	time       int
	plTime     int
	discount   string
}

func (q Query) getUrl() string {
	return fmt.Sprintf(urlTemplate, q.tp, q.plan, q.distance, q.plDistance, q.time, q.plTime, q.discount)
}

func (q Query) requestServer() (status int, price float64) {
	resp, err := http.Get(q.getUrl())
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	rgx, _ := regexp.Compile("{\"price\":([.0-9]+)}")
	match := rgx.FindStringSubmatch(string(body))
	if len(match) != 2 {
		return 400, 0
	}
	price, err = strconv.ParseFloat(match[1], 64)
	if err != nil {
		panic(err)
	}
	return 200, price
}

func (q Query) requestLocal() (status int, price float64) {
	// R1
	if q.tp == "budget" && q.plan == "fixed_price" && q.distance > 0 && q.plDistance > 0 && q.time > 0 && q.plTime > 0 && q.discount == "yes" {
		fmt.Printf("R1\n")
		if (float64(q.distance)/float64(q.plDistance)-1)*100 <= DeviationPercent {
			return 200, float64(fixedPriceKm*q.plDistance) * (1 - float64(Discount)/100)
		} else {
			return 200, float64(budgetPriceMin*q.time) * (1 - float64(Discount)/100)
		}
	}
	// R2
	if q.tp == "budget" && q.plan == "fixed_price" && q.distance > 0 && q.plDistance > 0 && q.time > 0 && q.plTime > 0 && q.discount == "no" {
		fmt.Printf("R2\n")
		if (float64(q.distance)/float64(q.plDistance)-1)*100 <= DeviationPercent {
			return 200, float64(fixedPriceKm * q.plDistance)
		} else {
			return 200, float64(budgetPriceMin * q.time)
		}
	}
	// R3
	if q.tp == "budget" && q.plan == "minute" && q.distance > 0 && q.plDistance > 0 && q.time > 0 && q.plTime > 0 && q.discount == "yes" {
		fmt.Printf("R3\n")
		return 200, float64(budgetPriceMin*q.time) * (1 - float64(Discount)/100)
	}
	// R4
	if q.tp == "budget" && q.plan == "minute" && q.distance > 0 && q.plDistance > 0 && q.time > 0 && q.plTime > 0 && q.discount == "no" {
		fmt.Printf("R4\n")
		return 200, float64(budgetPriceMin * q.time)
	}
	// R5
	if q.tp == "luxury" && q.plan == "minute" && q.distance > 0 && q.plDistance > 0 && q.time > 0 && q.plTime > 0 && q.discount == "yes" {
		fmt.Printf("R5\n")
		return 200, float64(luxuryPriceMin*q.time) * (1 - float64(Discount)/100)
	}
	// R6
	if q.tp == "luxury" && q.plan == "minute" && q.distance > 0 && q.plDistance > 0 && q.time > 0 && q.plTime > 0 && q.discount == "no" {
		fmt.Printf("R6\n")
		return 200, float64(luxuryPriceMin * q.time)
	}
	return 400, 0
}

func printHeader() {
	fmt.Printf("| Test case | Type | Plan | Distance | Planned Distance | Time | Planned Time | Discount | Result | Expected |\n|---|---|---|---|---|---|---|---|---|---|\n")
}

func printRow(idx int, q Query, result, expected string) {
	fmt.Printf("| %d | %s | %s | %d | %d | %d | %d | %s | %s | %s |\n", idx, q.tp, q.plan, q.distance, q.plDistance, q.time, q.plTime, q.discount, result, expected)
}

func main() {
	idx := 0
	printHeader()

	for _, tp := range []string{"luxury", "budget", "nonsense"} {
		for _, plan := range []string{"minute", "fixed_price", "nonsense"} {
			for _, distance := range []int{0, 1} {
				for _, plDistance := range []int{0, 1} {
					for _, time := range []int{0, 1} {
						for _, plTime := range []int{0, 1} {
							for _, discount := range []string{"yes", "no"} {
								q := Query{
									tp:         tp,
									plan:       plan,
									distance:   distance,
									plDistance: plDistance,
									time:       time,
									plTime:     plTime,
									discount:   discount,
								}
								resStatus, resPrice := q.requestServer()
								expStatus, expPrice := q.requestLocal()

								result := "Invalid Request"
								if resStatus == 200 {
									result = fmt.Sprintf("%.02f", resPrice)
								}
								expected := "Invalid Request"
								if expStatus == 200 {
									expected = fmt.Sprintf("%.02f", expPrice)
								}
								printRow(idx, q, result, expected)

								idx++
							}
						}
					}
				}
			}
		}
	}
}
